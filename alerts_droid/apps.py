# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class AlertsDroidConfig(AppConfig):
    name = 'death_star.alerts_droid'
    verbose_name = 'Alerts Droid'

    def ready(self):
        import death_star.alerts_droid.signals.handlers


