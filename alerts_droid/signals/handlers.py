# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json
from django.db.models.signals import m2m_changed, post_save, post_init, pre_save
from django.dispatch import receiver
from death_star.alerts_droid.tasks import set_check_metric
from death_star.alerts_droid.models import GroupCheckMetric, TemplateCheck, DashboardAlertCheck

import logging
adhlog = logging.getLogger('alert_dorid')


@receiver(m2m_changed, sender=GroupCheckMetric.list_template_check.through)
def list_template_check_changed(instance, sender, action, **kwargs):
    adhlog.debug('Start signal m2m_changed def list_template_check_changed sebder: {} args: {}'.format(sender, (instance, action, kwargs)))
    if action in ['post_add', 'post_remove']:
        templates = kwargs['model'].objects.filter(pk__in=kwargs['pk_set'])
        adhlog.debug('List templates list_template_check_changed: {}'.format(templates))
        nodes = list(instance.node_in_goup.all())
        hws = list(instance.hw_in_goup.all())
        if action == 'post_add':
            set_check_metric.delay(templates, nodes+hws, 'add', instance)
        elif action == 'post_remove':
            set_check_metric.delay(templates, nodes+hws, 'del', instance)


@receiver(m2m_changed, sender=GroupCheckMetric.node_in_goup.through)
@receiver(m2m_changed, sender=GroupCheckMetric.hw_in_goup.through)
def list_nodes_changed(instance, sender, action, **kwargs):
    adhlog.debug('Start signal m2m_changed list_nodes_changed sebder: {} args: {}'.format(sender, (instance, action, kwargs)))
    if action in ['post_add', 'post_remove']:
        hws = kwargs['model'].objects.filter(pk__in=kwargs['pk_set'])
        templates = instance.list_template_check.all()
        if action == 'post_add':
            set_check_metric.delay(templates, hws, 'add', instance)
        elif action == 'post_remove':
            set_check_metric.delay(templates, hws, 'del', instance)


@receiver(post_save, sender=DashboardAlertCheck)
def add_dashboar_write(sender, instance, **kwargs):
    if kwargs['created'] == True:
        msg = instance.gen_msg_error()
        instance.send_msg(msg)
    elif instance.state == True and kwargs['created'] == False:
        msg = instance.get_msg_good()
        instance.send_msg(msg)
    else:
        pass
        #еще не придумал
