# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import ast
from datetime import timedelta
from django.db import models as m
from django.db.models import QuerySet
from death_star.include.task_manager import TaskManager
from django.utils.timezone import now
from death_star.rebel_camp.models import Node, NetworkHardware
from django.core.exceptions import ObjectDoesNotExist
from death_star.celery import app

import logging
logger = logging.getLogger(__name__)

class TemplateCheck(m.Model):

    COMPARISON_TYPE = (
        (1, '>'),
        (2, '<'),
        (3, '=='),
        (4, '>='),
        (5, '<='),
        (6, '!='),
    )

    TYPE_CHECK = (
        (1, 'internal'),
        (2, 'external'),
    )

    LEVEL_ALERT = (
        (1, 'Low'),
        (2, 'Middle'),
        (3, 'High'),
    )

    TYPE_HW = (
        (1, 'Node'),
        (2, 'Network'),
    )

    class Meta:
        unique_together = ('name_check',)

    name_check = m.CharField(max_length=50)
    type_hw = m.IntegerField(default=1, choices=TYPE_HW)
    field_check = m.CharField(max_length=50)
    compare_oper_values = m.IntegerField(default=1, choices=COMPARISON_TYPE)
    value_alarm = m.FloatField(default=0)
    type_check = m.IntegerField(default=2, choices=TYPE_CHECK)
    level_alert = m.IntegerField(default=2, choices=LEVEL_ALERT)
    task_name = m.CharField(max_length=50, null=True, blank=True)
    kwargs = m.CharField(max_length=200, default={}, null=True, blank=True)
    send_msg = m.BooleanField(default=False)
    msg_to_alert = m.CharField(max_length=200, null=True, blank=True)
    timeout_check = m.IntegerField(default=60)


    def __init__(self, *args, **kwargs):
        super(TemplateCheck, self).__init__(*args, **kwargs)

    def get_cm_object(self, hw_object, op='add'):
        cm = CheckMetric.objects.none()
        if hw_object.__class__.__name__ == 'Node' and self.type_hw == 1:
            try:
                cm = CheckMetric.objects.get(node=hw_object, template_check=self)
            except ObjectDoesNotExist:
                if op == 'add':
                    cm = CheckMetric.objects.create(node=hw_object, template_check=self)
        elif hw_object.__class__.__name__ == 'NetworkHardware' and self.type_hw == 2:
            try:
                cm = CheckMetric.objects.get(net_hw=hw_object, template_check=self)
            except ObjectDoesNotExist:
                if op == 'add':
                    cm = CheckMetric.objects.create(net_hw=hw_object, template_check=self)
        return cm

    def task_mgr(self, hostname, name_task, timeout_check, op='add'):
        logger.info('template_check task_mgr args {}'.format((hostname, name_task, timeout_check, op)))
        tm = TaskManager(name_task, 'internal_check', timeout_check, 'CollectWrite')
        if op == 'add':
            kw_dict= ast.literal_eval(self.kwargs)
            kw_dict['hostname'] = hostname
            tm.create_task_checks(**kw_dict)
        elif op == 'del':
            tm.delete_task(name_task)
        elif op == 'disable':
            tm.disable_task(name_task)
        del tm
        logger.info('template_check end task_mgr ')

    def gen_task_name(self, hostname):
        return '{} {}'.format(self.name_check, hostname)

    def add_to_node(self, hw_object):
        cm = self.get_cm_object(hw_object, 'add')
        if cm:
            cm.name_check = self.name_check
            cm.field_check = self.field_check
            cm.compare_oper_values = self.compare_oper_values
            cm.type_check = self.type_check
            cm.level_alert = self.level_alert
            cm.value_alarm = self.value_alarm
            cm.timeout_check = self.timeout_check
            cm.msg_to_alert = self.msg_to_alert
            cm.task_name = self.task_name
            cm.is_delete = False
            cm.save()
            if cm.get_type_check_display() == 'internal':
                self.task_mgr(hw_object.hostname, self.gen_task_name(hw_object.hostname), self.timeout_check, 'add')
        return cm.id

    def del_to_node(self, hw_object):
        cm = self.get_cm_object(hw_object, 'del')
        if cm:
            cm.is_delete = True
            cm.save()
            if cm.get_type_check_display() == 'internal':
                self.task_mgr(hw_object.hostname, self.gen_task_name(hw_object.hostname), self.timeout_check, 'del')
        return cm.id

    def disable_to_node(self, hw_object):
        cm = self.get_cm_object(hw_object, 'del')
        logger.info('template_check get_cm_object = {}'.format(cm))
        if cm:
            cm.state_check = False
            cm.save()
            if cm.get_type_check_display() == 'internal':
                logger.info('template_check cm_object internal')
                self.task_mgr(hw_object.hostname, self.gen_task_name(hw_object.hostname), self.timeout_check, 'disable')
        return cm.id


    def __str__(self):
        return self.name_check


class CheckMetric(m.Model):

    COMPARISON_TYPE = (
        (1, '>'),
        (2, '<'),
        (3, '=='),
        (4, '>='),
        (5, '<='),
        (6, '!='),
    )

    TYPE_CHECK = (
        (1, 'internal'),
        (2, 'external'),
    )

    LEVEL_ALERT = (
        (1, 'Low'),
        (2, 'Middle'),
        (3, 'High'),
    )

    node = m.ForeignKey(Node, blank=True, null=True)
    net_hw = m.ForeignKey(NetworkHardware, blank=True, null=True)
    template_check = m.ForeignKey(TemplateCheck)
    name_check = m.CharField(max_length=50)
    field_check = m.CharField(max_length=50)
    value_check = m.FloatField(default=0)
    compare_oper_values = m.IntegerField(default=1, choices=COMPARISON_TYPE)
    value_alarm = m.FloatField(default=0)
    type_check = m.IntegerField(default=2, choices=TYPE_CHECK)
    level_alert = m.IntegerField(default=2, choices=LEVEL_ALERT)
    task_name = m.CharField(max_length=50, null=True, blank=True)
    kwargs = m.CharField(max_length=200, default={}, null=True, blank=True)
    send_msg = m.BooleanField(default=False)
    msg_to_alert = m.CharField(max_length=200, null=True, blank=True)
    timeout_check = m.IntegerField(default=60)
    datetime_add = m.DateTimeField(default=now)
    state_check = m.BooleanField(default=True)
    is_delete = m.BooleanField(default=False)

    def __str__(self):
        return '{} {} {} {} {}'.format(self.node.hostname if self.template_check.type_hw == 1 else self.net_hw.hostname,
            self.name_check,
            self.get_compare_oper_values_display(),
            self.value_alarm,
            'Active' if not self.is_delete else 'Deleted',
        )

    def is_alert_metric(self, value):
        if self.get_compare_oper_values_display() == '>':
            return value > self.value_alarm
        elif self.get_compare_oper_values_display() == '<':
            return value < self.value_alarm
        elif self.get_compare_oper_values_display() == '==':
            return value == self.value_alarm
        elif self.get_compare_oper_values_display() == '>=':
            return value >= self.value_alarm
        elif self.get_compare_oper_values_display() == '<=':
            return value <= self.value_alarm
        elif self.get_compare_oper_values_display() == '!=':
            return value != self.value_alarm
        else:
            return False

    def disable_metric(self):
        logger.info('start disable_metric')
        hw_obj = None
        if self.node:
            hw_obj = self.node
        elif self.net_hw:
            hw_obj = self.net_hw
        logger.info('disable_metric hw_obj = {}'.format(hw_obj))
        self.template_check.disable_to_node(hw_obj)
        logger.info('template_check end disable_to_node hw_obj = {}'.format(hw_obj))
        self.end_alert()
        logger.info('template_check end_alert')


    def check_metric(self):
        if self.state_check:
            if self.is_alert_metric(self.value_check):
                alert, dash_list = self.is_activ_alert()
                if not alert:
                    self.create_alert()
                    return False
                else:
                    pass
            elif not self.is_alert_metric(self.value_check) and self.is_activ_alert():
                self.end_alert()
                return True
            else:
                return None
        else:
            return None

    def is_activ_alert(self):
        alert = DashboardAlertCheck.objects.filter(check_alert=self, state=False)
        if alert:
            return True, alert
        else:
            return False, alert

    def create_alert(self):
        dac = DashboardAlertCheck.objects.create(check_alert=self, state=False)
        dac.save()

    def end_alert(self):
        dash = DashboardAlertCheck.objects.filter(check_alert=self, state=False)
        for i in dash:
            i.state=True
            i.datetime_end = now()
            i.save()

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        super(CheckMetric, self).save(force_insert, force_update, using, update_fields)
        self.check_metric()



class DashboardAlertCheck(m.Model):

    BOT_TOKEN = ''
    CHANNEL_ID = ''

    check_alert = m.ForeignKey(CheckMetric)
    state = m.BooleanField(default=False)
    datetime_add = m.DateTimeField(default=now)
    datetime_send_notify = m.DateTimeField(default=now)
    datetime_end = m.DateTimeField(default=now)
    description = m.CharField(max_length=1000, null=True, blank=True)
    in_work = m.BooleanField(default=False)
    is_delete = m.BooleanField(default=False)

    def __str__(self):
        return '{} {} {}'.format(self.check_alert.name_check, self.state, self.datetime_add)

    def is_repeat_send(self):
        if now() - self.datetime_send_notify > timedelta(minutes=30):
            return True
        else:
            return False

    def gen_msg_error(self):
        msg = '%F0%9F%98%A1 CREATE ALARTM {id}!!!\n' \
              'On the node {node} rack:{r} unit:{u}\n' \
              'parameter {field} {comp} {value_alarm}.\n' \
              'Present value: {curr_value}'
        return msg.format(
            id=self.id,
            node=self.check_alert.node.hostname,
            r=self.check_alert.node.rack,
            u=self.check_alert.node.unit2,
            field=self.check_alert.field_check,
            comp=self.check_alert.get_compare_oper_values_display(),
            value_alarm=self.check_alert.value_alarm,
            curr_value=round(self.check_alert.value_check, 2)
        )

    def get_msg_good(self):
        msg = '%F0%9F%98%81 CLOSED ALARM {id}\n' \
              'On the node {node} rack:{r} unit:{u}\n' \
              'parameter {field} {comp} {value_alarm}.\n' \
              'Present value: {curr_value}'
        return msg.format(
            id=self.id,
            node=self.check_alert.node.hostname,
            r=self.check_alert.node.rack,
            u=self.check_alert.node.unit2,
            field=self.check_alert.field_check,
            comp=self.check_alert.get_compare_oper_values_display(),
            value_alarm=self.check_alert.value_alarm,
            curr_value=round(self.check_alert.value_check, 2)
        )

    def send_msg(self, msg):
        app.send_task('send_tm_msg', args=[self.BOT_TOKEN, self.CHANNEL_ID, msg], kwargs={})


class GroupCheckMetric(m.Model):

    TYPE_HW = (
        (1, 'Node'),
        (2, 'Network'),
    )

    class Meta:
        unique_together = ('name_group',)

    name_group = m.CharField(max_length=100)
    type_hardware = m.IntegerField(default=1, choices=TYPE_HW)

    list_template_check = m.ManyToManyField(TemplateCheck, blank=True)
    node_in_goup = m.ManyToManyField(Node, blank=True)
    hw_in_goup = m.ManyToManyField(NetworkHardware, blank=True)
    check_metric_in_group = m.ManyToManyField(CheckMetric, related_name='activ_metric', blank=True)

    def get_hw_list(self, type_hw):
        if type_hw == 'Node':
            return self.node_in_goup.all()
        elif type_hw == 'Network':
            return self.hw_in_goup.all()
        else:
            return []

    def get_group_templ_metrics(self):
        return self.list_template_check.all()

    def __str__(self):
        return self.name_group


class NotificationQueue(m.Model):

    name = m.CharField(max_length=100)
    msg = m.CharField(max_length=1000)
    datetime_add = m.DateTimeField(default=now)

    def __str__(self):
        return self.name



