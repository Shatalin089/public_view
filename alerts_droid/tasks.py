# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import requests
from death_star.celery import app
from death_star.alerts_droid.models import *
from django.core.exceptions import ObjectDoesNotExist

import logging
logger = logging.getLogger(__name__)


@app.task(name='set_check_metric', queue='Alerts')
def set_check_metric(template_list, hw_list, op, gcm=None):
    logger.info('set_check_metric args = {args}'.format(args=(template_list, hw_list, op, gcm)))
    logger.info('current check metric in group {args}'.format(args=gcm.check_metric_in_group.all()))
    for hw in hw_list:
        for template in template_list:
            try:
                if op == 'add':
                    cm_id = template.add_to_node(hw)
                    logger.info('set_check_metric add id {args}'.format(args=cm_id))
                    gcm.check_metric_in_group.add(cm_id)
                    logger.info('gcm add {args}'.format(args=gcm))
                elif op == 'del':
                    cm_id = template.del_to_node(hw)
                    logger.info('set_check_metric remove id {args}'.format(args=cm_id))
                    gcm.check_metric_in_group.remove(cm_id)
                    logger.info('gcm remove {args}'.format(args=gcm))
            except Exception as er:
                logger.error('set_check_metric error {args}'.format(args=er))
    gcm.save()
    logger.info('after change check metric in groupe {args}'.format(args=gcm.check_metric_in_group.all()))
    return True

@app.task(name='send_tm_msg', queue='Alerts')
def send_tm_msg(bot_token, chat_id, msg):
    url = "https://api.telegram.org/bot{token}/sendMessage?chat_id={chat_id}&text={text}"
    res = requests.get(url.format(token=bot_token, chat_id=chat_id, text=msg))
    return res.text

@app.task(name='check_metric', queue='Alerts')
def check_metric(*args):
    check_object = args[0][0]
    field = args[0][1]
    value = args[0][2]
    try:
        if check_object.__class__.__name__ == 'Node':
            cm = CheckMetric.objects.get(node=check_object, field_check=field)
        elif check_object.__class__.__name__ == 'NetworkHardware':
            cm = CheckMetric.objects.get(net_hw=check_object, field_check=field)
        else:
            return 'Not correct check object'
    except ObjectDoesNotExist:
        return 'ObjectDoesNotExist'
    cm.value_check = value
    cm.save()
    return cm.check_metric()

@app.task(name='dis_metr', queue='Alerts')
def dis_metr(cm_obj):
    cm_obj.disable_metric()