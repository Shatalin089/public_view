# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.mixins import PermissionRequiredMixin
from django.shortcuts import render

# Create your views here.
from django.urls import reverse_lazy
from django.views.generic import ListView
from django.views.generic.edit import BaseUpdateView
from death_star.alerts_droid.models import CheckMetric
from death_star.alerts_droid import models as m
from death_star.celery import app


class AlarmsView(PermissionRequiredMixin, ListView):
    permission_required = 'droid_collectors.alarms_view'
    model = m.DashboardAlertCheck
    template_name = 'alarms_list.html'
    context_object_name = 'alarms'

    def get_queryset(self):
        return self.model.objects.filter(state=False).order_by('-datetime_add')

    def get(self, request, *args, **kwargs):
        if '/alerts/check_metric/disable/' in request.path:
            cm = CheckMetric.objects.get(pk=kwargs['pk'])
            app.send_task('dis_metr', args=[cm], kwargs={})
        return super(AlarmsView, self).get(request, *args, **kwargs)
