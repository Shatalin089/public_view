from django.conf.urls import url
from death_star.alerts_droid import views as v
from django.contrib.auth.decorators import login_required


urlpatterns = [
    url(r'^view/$', login_required(v.AlarmsView.as_view(), login_url='/'), name='aletrs_view'),
    url(r'^check_metric/disable/(?P<pk>[0-9]+)/$', login_required(v.AlarmsView.as_view(), login_url='/'), name='cm_disable'),
    ]
